# Time series generation and manipulation

Allow to generate and manipulate time series, according to the ARMA (auto-regressive and moving average) models.

In particular, it allows any AR(p) model with any p-integer, using the `ar` function, and any ARFI(p,d) (fractionally integrated auto-regressive model) with integer p and float d fractional derivative generation, using the `arfima` function.

AR models can be differentiate using the nabla operator to power n, function `nabla`.

A fractional white noise time series is constructed using the Harte and Davies algorithm : 
    
*  [Davies and Harte, Biometrika 74 p.95–101 (1987)](http://dx.doi.org/10.2307/2336024)
*  [Craigmile, Journal of Time Series Analysis 24 p.505-511 (2003)](http://dx.doi.org/10.1111/1467-9892.00318)

using the `frac_rand` function.

The fractional differentiation can be done using either the Jensen and Nielsen algorithm (function `fracdiff`): 

*  [Jensen and Nielsen, Journal of time series analysis 35 p.426-438 (2014)](https://doi.org/10.1111/jtsa.12074)

or using an heuristic evaluation of the cut-off used to truncate the recursivity of the differentiation (function `nablafrac`).

**Limitations :** the series are generated using a white noise with mean 0 and variance 1.

See the demo to learn how to use the different functions, and their options. 