import timeseries as ts
import matplotlib.pyplot as plt

# generate and plot an AR(4) time series
x = ts.ar( {1:0.2, 4:0.01}, N=1000 ) 
    # change the dictionnary in the form {power:coefficient, ...} to change the series
    # N is the size of the resulting series
plt.plot(x)
plt.show()
plt.clf()

# generate and plot an AR(8,0.2) time series
y = ts.arfima( {1:0.1, 8:0.3}, d=0.2, N=5000 )
    # change the dictionnary in the form {power:coefficient, ...} to change the series
    # change the -1/2 < d < 1/2 float coefficient to change the fractional order
    # N is the size of the resulting series
plt.plot(y)
plt.show()
plt.clf()

# generate and plot an AR(1) process, and its first difference operator
z = ts.ar( {1:1.01}, 500 )
z1 = ts.nabla(z,n=1)
    # change n to change the order of differentiation
plt.plot(z)
plt.plot(z1)
plt.show()
plt.clf()

# generate and plot a fractional white noise process
f = ts.frac_rand( d=0.2, N=500 )
    # d is the fractional power of the series
    # N is the size of the series
plt.plot(f)
plt.show()
plt.clf()

# fractional differentiation of the y time series, and comparison between fracdiff and nablafrac methods
d = 0.2
L = ts.phi( d, pres=1e-3 )
    # phi is a function returning the size of the series which will be withdrawn from the initial series
    # the precision defines the cut-off of the algorithm 
dy1 = ts.fracdiff( y, d )
    # fracdiff gets a 1D series and a float d as the order of the differentiation
dy2 = ts.nablafrac( y, d, pres=1e-3 )
    # nablafrac gets a third parameter, corresponding to the precision used to cut the initial series
plt.plot(dy1[L:])
plt.plot(dy2)
    # check the first 50 terms of the two series
    # the dy1 series is bigger than dy2, because dy2 withdraw the first terms of the series to calculate the fractional differentiate
print(dy1[L:L+50])
print(dy2[0:50])

# squared difference between the two series
s = 0.0
    # the calculation is equivalent to numpy.sum((dy1[L:]-dy2)**2) when using numpy
for i in range(len(dy2)):
    s += (dy1[L+i]-dy2[i])**2
print(s)