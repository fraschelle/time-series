from math import gamma
import numpy as np

# GENERATE TIME SERIES

def rand(q=0):
    """
    Returns a white noise sample with mean 0 and variance 1
    if q=0, use a uniform distribution
    else, use a Gaussian distribution
    """
    if q ==0:
        return(np.sqrt(12)*(np.random.random()-0.5))
    else:
        return(np.random.randn())

def ar(dict_poly,N=1000):
    """
    Generate an AR process with parameters given in a dictionnary.
    Input : 
        dict_poly : a dictionnary {order_of_the_polynomial : coefficient}
            order_of_the_polynomial : an integer, the power of the back-traking operator
            coefficient : a float / the coefficient itself
    Output : a 1D numpy series
    """
    x = [i for i in dict_poly.keys()]
    p = np.max(x)
    X = [rand() for i in range(p)]
    for i in range(p,N):
    	tmp_alpha = [dict_poly[i]*X[-i] for i in dict_poly]
    	X = np.append(X, np.sum(tmp_alpha) + rand())
    return(X)

def arma_rand(p,q, a=1.0, b=1.0, N=1000):
    """
    Generate an ARMA(p,q) process of size N where a and b are coefficient multiplying the random numbers
    """
    # generate the alpha and beta coefficients
    alpha = [a*rand() for i in range(p)]
    beta  = [b*rand() for i in range(q)]
    # initialize the sequence with white noise
    X = [b*rand() for i in range(p)]
    # generate the rest of the sequence
    for i in range(p,N):
    	tmp_alpha = [alpha[i]*X[-(i+1)] for i in range(p)]
    	tmp_beta  = [i*rand() for i in beta]
    	X = np.append(X, np.sum(tmp_alpha) + np.sum(tmp_beta))
    return(X)

def fract_rand(d,N=1000):
    """
    Generates a series of fractional white noise process, using the Davies and Harte algorithm.
        - Davies and Harte, Biometrika 74 p.95–101 (1987) 
            -> http://dx.doi.org/10.2307/2336024
        - Craigmile, Journal of Time Series Analysis 24 p.505-511 (2003) 
            -> http://dx.doi.org/10.1111/1467-9892.00318
    Time step between two events is supposed to be 1.
    Input : 
        -1/2 < d < 1/2 : a float, the fractional dimension of the process
        N > 0 : an integer, the size of the series
    Output : 
        a list of size N
    """
    r = [gamma(1-2*d)/gamma(1-d)**2]
    for i in range(1,N):
        r.append((i-1+d)/(i-d)*r[-1])
    r_temp = r[::-1]
    r = np.append(np.append(r,0.0),r_temp[:-1:])
    l = np.sqrt(np.fft.fft(r).real)
    x = [rand()]
    for i in range(1,N):
        x.append((rand()+rand()*1j)/np.sqrt(2))
    x_temp = np.conj(x[::-1])
    x = np.append(np.append(x,rand()),x_temp[:-1:])
    y = np.fft.fft(l*x/np.sqrt(2*N)).real
    return(y[:N])

def arfima(dict_poly,d,N=1000):
    """
    Generate an ARFIMA time series with parameters given in a dictionnary, whith a fractional white noise.
    Input : 
        dict_poly : a dictionnary {order_of_the_polynomial : coefficient}
            order_of_the_polynomial : an integer, the power of the back-traking operator
            coefficient : a float / the coefficient itself
        -1/2 < d < 1/2 : a float, the fractional dimension of the process
        N > 0 : an integer, the size of the series
    Output : a 1D numpy series
    """
    x = [i for i in dict_poly.keys()]
    p = np.max(x)
    X = fract_rand(d,p)
    for i in range(p,N):
    	tmp_alpha = [dict_poly[i]*X[-i] for i in dict_poly]
    	X = np.append(X, np.sum(tmp_alpha) + fract_rand(d,1))
    return(X)

# MANIPULATE TIME SERIES

def nabla(X,n):
    """
    Apply a filter (1-B)**n of order n to a time series, with B the backshift operator.
    Suppress the first n terms of the initial series.
    Input : 
        X : a list ar a 1D numpy.array
        n : an integer
    Output : a numpy.array of size len(X)-n
    """
    Z = np.zeros(len(X)-n)
    for i in range(n,len(X)):
        z = [(-1)**k*gamma(n+1)/gamma(k+1)/gamma(n-k+1)*X[i-k] for k in range(0,n+1)]
        Z[i-n] = np.sum(z)
    return(Z)

def phi(z, pres=1e-3):
    """
    Evaluate the number of iterations required to get the fractional differentiation up to some precision.
    The precision criterion is evaluated as 'precision' below. 
    Other parameterization could be used.
    """
    p = [1]
    precision = 1
    l = 1
    while precision >= pres:
        p.append((l-z-1)/l*p[-1])
        l += 1
        precision = np.abs((p[-1]-p[-2])/p[-1])
    return(len(p))

def nablafrac(X,d,pres=1e-3):
    """
    Fractional differentiation of order d of a time series X, with precision of pres in convergence.
    The precision is evaluated according to the function 'phi'.
    The initial series is truncated according to the precision evaluation.
    Fractional differentiation is defined as in Hosking
        - Hosking, Biometrika 68 p.165–176 (1981)
            -> https://doi.org/10.1093/biomet/68.1.165
    If the initial time series is too short, a warning message appears, and the result is an empty series.
    """
    N = phi(d,pres)
    if N >= len(X):
        print('Impossible to apply the fractional differentiation')
        res = []
    else:
        coeff = np.arange(1,N)
        coeff = np.cumprod((coeff-1-d)/coeff)
        coeff = np.append(1,coeff)
        res = [np.sum(coeff[::-1]*X[i-N+1:i+1]) for i in range(N,len(X))]
    return(np.array(res))

def fracdiff(x,d):
    """
    Apply a fractional differentiation of order d to a time series x (a 1D numpy.array)
    Does not withdraw the first terms of the series, since it uses a circular convolution trick and some fast Fourier transform algorithm due to Jensen and Nielsen : 
        - Jensen and Nielsen, Journal of time series analysis 35 p.426-438 (2014)
            -> https://doi.org/10.1111/jtsa.12074
    So the first terms are untrusty in the following. 
    For a trustfull algorithm, use 'nablafrac' instead.
    Source : 
        https://www.patreon.com/posts/fractional-21878207
        https://github.com/SimonOuellette35/FractionalDiff/blob/master/question2.py
    """
    T = len(x)
    N = int(2**np.ceil(np.log2(2*T-1)))
    k = np.arange(1,T)
    b = (1,) + tuple(np.cumprod((k-d-1)/k))
    z = (0,)*(N-T)
    z1 = b + z
    z2 = tuple(x) + z
    dx = np.fft.ifft(np.fft.fft(z1)*np.fft.fft(z2))
    X = np.real(dx[0:T])
    return(X)
